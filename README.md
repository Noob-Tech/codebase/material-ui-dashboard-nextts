# Material UI Dashboard Template using `NextTS`

## Prerequisites

1. Node.js v12

## Set-up

### Development Config

1. Copy [.env.example](.env.example) as [.env](.env)

```bash
cp ./.env.example ./.env
```

2. Belows are available configurations:

| Key              | Description       | Required | Values |
|------------------|-------------------| ----- | ----------- |
| `ENCRYPT_SECRET` | Encryption secret | **✓** | String |


### Install Dependencies

```bash
npm install
```

## Development

### Start App Development Runtime

```bash
npm run dev
```

### Contributors ###

- Alfarih Faza <alfarihfz@gmail.com>
