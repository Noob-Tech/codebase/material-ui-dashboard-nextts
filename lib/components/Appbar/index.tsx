import React from 'react';
import type { ReactElement } from 'react';
import classNames from 'classnames';
import { AppBar, Toolbar, useScrollTrigger, IconButton } from '@mui/material';
import { Menu } from '@mui/icons-material';

function ElevationScroll({ children, window }: { children: ReactElement; window: Window }) {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 5 : 0,
  });
}

export default function Appbar({
  drawerOpen = false,
  toggleDrawer,
  ...props
}: {
  drawerOpen: boolean;
  toggleDrawer: () => void;
} & KeyValue) {
  return (
    <>
      <ElevationScroll window={props.window}>
        <AppBar position="fixed" className={classNames('AppBar', { DrawerOpen: drawerOpen })}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="AppBar AppDrawer"
              onClick={toggleDrawer}
              edge="start"
              className="MenuIcon"
            >
              <Menu />
            </IconButton>
            THIS WILL BE TOOLBAR
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <Toolbar />
    </>
  );
}
