import Link from 'next/link';
import { Typography } from '@mui/material';
import Img from 'next/image';
import Logo from '$assets/img/logo.png';

export default function DrawerHeader() {
  return (
    <div className="DrawerHeader">
      <Link href="/">
        <a href="/">
          <Img src={Logo} alt={process.env.APP_NAME} />
          <Typography variant="h6" className="DrawerHeader__Title">
            {process.env.APP_NAME}
          </Typography>
        </a>
      </Link>
    </div>
  );
}
