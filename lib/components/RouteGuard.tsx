import { ReactElement, useEffect, useMemo, useState } from 'react';
import routeConfig, { loginPath } from '$configs/route.config';
import store from '$lib/stores';
import { useRouter } from 'next/router';
import { getSession } from '$lib/services/api/auth';
import PageLoader from '$components/PageLoader';
import Error from './Error';
import { updateSession } from '$lib/stores/actions';
import md5 from 'md5';

export const getPagePermission = (pathname: string, session?: Session): boolean => {
  if (pathname === loginPath) {
    return !session;
  }

  const c = routeConfig[pathname];
  if (!c || !c.permissions) {
    return true;
  }

  if (session) {
    return c.permissions.length > 0 && c.permissions.includes(session.role);
  }

  return c.permissions.length === 0;
};

const useRouterPath = () => {
  const router = useRouter();
  const [path, setPath] = useState(router.pathname);
  const listenToPopstate = () => {
    if (typeof window !== 'undefined') {
      return setPath(window.location.pathname);
    }
    setPath(router.pathname);
  };

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('popstate', listenToPopstate);
      return () => {
        window.removeEventListener('popstate', listenToPopstate);
      };
    }

    router.events.on('routeChangeComplete', listenToPopstate);

    return () => {
      router.events.off('routeChangeComplete', listenToPopstate);
    };
  }, []);

  return path;
};

const useRouterSession = (): [Session, string] => {
  const path = useRouterPath();
  const [session, setSession] = useState(store.getState().session);

  store.subscribe(() => {
    const { session: recentSession } = store.getState();
    if (md5(JSON.stringify(session)) !== md5(JSON.stringify(recentSession))) {
      setSession(recentSession);
    }
  });

  useEffect(() => {
    getSession().then((s) => updateSession(s));
  }, [path]);

  return [session, path];
};

export default function RouteGuard({ children }: { children: ReactElement }) {
  const [_children, setChildren] = useState(<PageLoader />);
  const router = useRouter();
  const [session, path] = useRouterSession();

  async function authCheck(): Promise<ReactElement> {
    // Only handle on client side
    if (typeof window !== 'undefined') {
      // Get page permission
      const isAllowed = getPagePermission(path, session);

      if (!isAllowed) {
        // Handle for authenticated user
        if (session && session.authenticated) {
          if (path === loginPath) {
            return <Error statusCode={403} message="Forbidden" />;
          }
          return <Error statusCode={401} message="Unauthorized" />;
        }

        // Handle for unauthenticated user
        if (path !== loginPath) {
          router.replace({ pathname: loginPath }, undefined, { shallow: true });
          return <PageLoader />;
        }
      }

      return children;
    }

    return <PageLoader />;
  }

  useMemo(authCheck, [session, path]).then((c) => setChildren(c));

  return _children;
}
