import HTTPClient from '$lib/http-client';

export const authClient = new HTTPClient({
  baseURL: '',
  basePath: '/api/auth',
  defaultHeaders: {},
});
