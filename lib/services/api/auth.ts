import { authClient } from '$lib/services/api/clients';
import { HTTP_METHOD } from '$lib/http-client';

export const login = async (username: string, password: string): Promise<Session> => {
  const res = await authClient.handleRequest<{ data: Session }>(HTTP_METHOD.POST, {
    path: '/login',
    data: {
      username,
      password,
    },
  });

  return res.data.data;
};

export const logout = async (): Promise<Session> => {
  await authClient.handleRequest<{ role: number }>(HTTP_METHOD.DELETE, {
    path: '/logout',
  });

  return {
    authenticated: false,
    role: 0,
    userID: 0,
  };
};

export const getSession = async (): Promise<Session> => {
  const res = await authClient.handleRequest<{ data: Session }>(HTTP_METHOD.GET, {
    path: '/session',
  });

  return res.data.data;
};
