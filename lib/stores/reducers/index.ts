import type { State } from '$dto/redux';
import { combineReducers } from 'redux';
import uiState from './ui.reducer';
import session from './session.reducer';

const reducers = combineReducers<State>({
  uiState,
  session,
});
export default reducers;
