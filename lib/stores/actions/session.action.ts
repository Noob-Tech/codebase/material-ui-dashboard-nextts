import { UPDATE_SESSION } from '../events';
import store from '../index';
import * as authAPI from '$lib/services/api/auth';

const { dispatch } = store;

export const updateSession = async (s: Session) => dispatch({ type: UPDATE_SESSION, payload: s });

export const login = async (username: string, password: string) => {
  const session = await authAPI.login(username, password);

  dispatch({
    type: UPDATE_SESSION,
    payload: session,
  });
};

export const logout = async () => {
  await authAPI.logout();

  dispatch({
    type: UPDATE_SESSION,
    payload: {
      authenticated: false,
      userID: 0,
      role: 0,
    },
  });
};
