import { applyMiddleware, createStore } from 'redux';
// eslint-disable-next-line import/no-extraneous-dependencies
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducers from './reducers';

const middlewares = [thunk];

const composedEnhancers = composeWithDevTools(applyMiddleware(...middlewares));

let initialState = {};
if (typeof window !== 'undefined') {
  initialState = window.__PRELOADED_STATE__ || {};
  delete window.__PRELOADED_STATE__;
}

const store = createStore(rootReducers, initialState, composedEnhancers);

export default store;
