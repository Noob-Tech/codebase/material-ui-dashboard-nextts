import type { IncomingMessage, ServerResponse } from 'http';
import { State } from '$dto/redux';
import { getSessionFromRequest } from '$lib/server/auth.server';

export default async function hydrate(req: IncomingMessage, res: ServerResponse): Promise<Partial<State>> {
  const s: Partial<State> = {};

  s.session = getSessionFromRequest(req, res);

  return s;
}
