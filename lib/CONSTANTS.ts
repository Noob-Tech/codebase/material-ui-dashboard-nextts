export const USER_ROLE = {
  PUBLIC: 0,
  PERSONAL: 1,
  ADMIN: 2,
};

export const COOKIES_KEY = {
  SERVER_SIDE_CLIENT_TOKEN: '__sct',
  CLIENT_SIDE_CLIENT_TOKEN: '__cct',
  USER_TOKEN: '__ut',
  SESSION: '__s',
};
