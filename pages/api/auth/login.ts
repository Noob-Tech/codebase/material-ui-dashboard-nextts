import type { NextApiRequest, NextApiResponse } from 'next';
import { HTTP_METHOD } from '$lib/http-client';
import { COOKIES_KEY, USER_ROLE } from '$lib/CONSTANTS';
import ServerCookies from '$lib/server-cookies';
import KeyGrip from '$lib/key-grip';

const post = async (req: NextApiRequest, res: NextApiResponse) => {
  const { username, password } = req.body;

  if (password !== 'password123') {
    return res.status(401).json({ message: 'wrong password' });
  }

  let role = USER_ROLE.PUBLIC;
  switch (username) {
    case 'admin':
      role = USER_ROLE.ADMIN;
      break;
    case 'user':
      role = USER_ROLE.PERSONAL;
      break;
    default:
      return res.status(401).json({ message: 'wrong username' });
  }

  const cookie = new ServerCookies(req, res, { encrypt: true, keys: new KeyGrip(process.env.ENCRYPT_SECRET || '') });

  cookie.setValue(COOKIES_KEY.USER_TOKEN, Buffer.from(username + password).toString('base64'), {
    expires: new Date(new Date().getTime() + 300000),
  });

  const payload: Session = { authenticated: true, role, userID: username };
  cookie.setValue(COOKIES_KEY.SESSION, JSON.stringify(payload));
  res.status(200).json({ message: 'OK', data: payload });
};

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method?.toUpperCase()) {
    case HTTP_METHOD.POST:
      return post(req, res);
    default:
      return res.status(404).send('not implemented');
  }
}
