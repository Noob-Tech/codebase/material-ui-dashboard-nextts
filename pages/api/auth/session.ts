import type { NextApiRequest, NextApiResponse } from 'next';
import { HTTP_METHOD } from '$lib/http-client';
import { getSessionFromRequest } from '$lib/server/auth.server';

const get = async (req: NextApiRequest, res: NextApiResponse) => {
  res.status(200).json({ message: 'OK', data: getSessionFromRequest(req, res) });
};

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method?.toUpperCase()) {
    case HTTP_METHOD.GET:
      return get(req, res);
    default:
      return res.status(404).send('not implemented');
  }
}
