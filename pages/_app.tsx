import type { NextRouter } from 'next/router';
import type { AppContext, AppInitialProps } from 'next/app';
import NextApp from 'next/app';
import { NextPage } from 'next';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { Provider } from 'react-redux';
import store from '$lib/stores';
import RouteConfig from '$configs/route.config';
import PageLoader from '$components/PageLoader';

function App({ pageProps, Component, router }: { pageProps: KeyValue; Component: NextPage; router: NextRouter }) {
  const config = RouteConfig[router.pathname];

  let Layout = dynamic(() => import('./_layout/FullPageLayout'), { loading: PageLoader });
  if (config) {
    if (!config.fullLayout && router.pathname !== '/error/[code]') {
      Layout = dynamic(() => import('./_layout/VerticalLayout'), { loading: PageLoader });
    }
  }

  const RouteGuard = dynamic(() => import('$components/RouteGuard'), { loading: PageLoader });

  return (
    <>
      <Head>
        <title>Material UI Dashboard</title>
      </Head>
      <Provider store={store}>
        <Layout>
          <RouteGuard>
            <Component {...pageProps} />
          </RouteGuard>
        </Layout>
      </Provider>
    </>
  );
}

App.getInitialProps = async (ctx: AppContext): Promise<AppInitialProps> => {
  const props: AppInitialProps = {
    pageProps: {},
  };

  Object.assign(props, await NextApp.getInitialProps(ctx));

  if (ctx.Component.getInitialProps) {
    Object.assign(props.pageProps, await ctx.Component.getInitialProps(ctx.ctx));
  }

  return props;
};

export default App;
