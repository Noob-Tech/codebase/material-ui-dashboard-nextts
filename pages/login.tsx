import { Button, Card, CardActions, CardContent, TextField, Typography } from '@mui/material';
import { useState } from 'react';
import { showSnackbar, login } from '$lib/stores/actions';
import Router from 'next/router';
import { homePath } from '$configs/route.config';

export default function LoginPage() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    try {
      await login(username, password);

      Router.replace({ pathname: homePath }, undefined, { shallow: true }).then(() => showSnackbar('Logged in...'));
    } catch (err: any) {
      showSnackbar(err.message, 'error');
    }
  };

  return (
    <div className="FlexContainer--Center" style={{ padding: '1rem' }}>
      <Card>
        <CardContent>
          <Typography variant="h5">Login</Typography>
          <Typography variant="h6">Welcome</Typography>
          <TextField
            label="Username"
            style={{ width: '100%' }}
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            margin="normal"
          />
          <TextField
            label="Password"
            style={{ width: '100%' }}
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            margin="normal"
          />
        </CardContent>
        <CardActions>
          <Button onClick={handleLogin}>Login</Button>
        </CardActions>
      </Card>
    </div>
  );
}
