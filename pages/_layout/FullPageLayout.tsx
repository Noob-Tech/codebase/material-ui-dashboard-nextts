import type { ReactElement } from 'react';
import Snackbar from '$components/Snackbar';

export default function FullPageLayout({ children }: { children: ReactElement }) {
  return (
    <>
      <main className="MainFullLayout">{children}</main>
      <Snackbar />
    </>
  );
}
