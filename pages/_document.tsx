import type { DocumentContext, DocumentInitialProps, DocumentProps } from 'next/document';
import NextDocument, { Html, Main, NextScript, Head } from 'next/document';
import { State } from '$dto/redux';
import hydrate from '$lib/stores/hydrate';

export default function Document(props: DocumentProps) {
  const { head } = props;
  return (
    <Html>
      <Head>
        <meta name="description" content="Material UI Dashboard Template" />
        <link rel="icon" href="/assets/ico/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        {/* eslint-disable-next-line @next/next/no-css-tags */}
        <link rel="stylesheet" href="/assets/css/core.css" />
        {head && head.map((h) => h)}
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}

Document.getInitialProps = async (ctx: DocumentContext): Promise<DocumentInitialProps> => {
  const initialProps = await NextDocument.getInitialProps(ctx);
  const { req, res } = ctx;

  const preloadedState: Partial<State> = {};
  if (req && res) {
    Object.assign(preloadedState, await hydrate(req, res));
  }

  initialProps.head = [
    <script
      key="__PRELOADED_STATE__"
      id="redux-state"
      dangerouslySetInnerHTML={{
        __html: `window.__PRELOADED_STATE__=${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}`,
      }}
    />,
  ];
  return initialProps;
};
